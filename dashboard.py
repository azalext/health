from controlcenter import Dashboard, widgets

from azalext.health.models import *

################################################################################

class CompanyList(widgets.ItemList):
    model = DrugCompany
    list_display = ('alias','title')

#*******************************************************************************

class FeatureList(widgets.ItemList):
    model = DrugFeature
    list_display = ('alias','title')

#*******************************************************************************

class SubstanceList(widgets.ItemList):
    model = DrugSubstance
    list_display = ('alias','title')

################################################################################

class MedicamentPrices(widgets.SingleBarChart):
    #queryset = Medicament.objects.aggregate(
    #    Avg('prix_c'),
    #    Max('prix_c'),
    #    Min('prix_c'),
    #)
    queryset = Medicament.objects.order_by('-prix_c')
    # label and series
    values_list = (
        'alias','prix_c'
    )

################################################################################

class Landing(Dashboard):
    title = 'Health'

    widgets = (
        MedicamentPrices,
        CompanyList,
        FeatureList,
        SubstanceList,
    )

