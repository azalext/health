# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

####################################################################

class FeatureAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(DrugFeature, FeatureAdmin)

#*******************************************************************************

class SubstanceAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(DrugSubstance, SubstanceAdmin)

####################################################################

class ProviderAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(DrugProvider, ProviderAdmin)

#*******************************************************************************

class CompanyAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(DrugCompany, CompanyAdmin)

####################################################################

class MedicamentAdmin(admin.ModelAdmin):
    list_display  = ['alias','title']
    #list_filter   = ['user__username']
    #list_editable = ['title']

admin.site.register(Medicament, MedicamentAdmin)

