# -*- coding: utf-8 -*-

from azalinc.shortcuts import *

from azalinc.node.models import *

#*********************************************************************

BACKEND_TYPEs = (
    ('cache', "Memcache daemon"),
    ('redis', "Redis or Sentinel"),

    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

################################################################################

class DrugFeature(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Pharmaceutical Feature"
        verbose_name_plural = "Pharmaceutical Features"

#*******************************************************************************

class DrugSubstance(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Pharmaceutical Substance"
        verbose_name_plural = "Pharmaceutical Substances"

################################################################################

class DrugProvider(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Pharmaceutical Distributor"
        verbose_name_plural = "Pharmaceutical Distributors"

#*******************************************************************************

class DrugCompany(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Pharmaceutical Company"
        verbose_name_plural = "Pharmaceutical Companies"

################################################################################

class DrugAnnuary(models.Model):
    alias    = models.CharField(max_length=64)
    title    = models.CharField(max_length=128)

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Pharmaceutical Company"
        verbose_name_plural = "Pharmaceutical Companies"

#*******************************************************************************

class Medicament(models.Model):
    maker  = models.ForeignKey(DrugCompany, blank=True, null=True, default=None, help_text="Fabriquant")
    where  = models.ForeignKey(DrugAnnuary, blank=True, null=True, default=None, help_text="Catalogue")
    alias  = models.CharField(max_length=64)
    title  = models.CharField(max_length=128, blank=True, help_text="Nom complet")

    brand  = models.ManyToManyField(DrugSubstance, blank=True, help_text="Famille")
    feats  = models.ManyToManyField(DrugFeature, blank=True, help_text="Tableau")
    order  = models.ManyToManyField(DrugProvider, blank=True, help_text="Distributeurs")

    code   = models.CharField(max_length=512, blank=True, help_text="Code ATC")
    info   = models.CharField(max_length=512, blank=True, help_text="Indications")
    mole   = models.CharField(max_length=512, blank=True, help_text="Composition")

    kind   = models.CharField(max_length=512, blank=True, help_text="Nature du Produit")
    pack   = models.CharField(max_length=512, blank=True, help_text=u'Pr\xe9sentation')
    cons   = models.CharField(max_length=512, blank=True, help_text="Conservation")
    ages   = models.CharField(max_length=512, blank=True, help_text="Age minimal d'utilisation")

    prix_c = models.CharField(max_length=512, blank=True, help_text="P.P.C")
    prix_v = models.CharField(max_length=512, blank=True, help_text="P.P.V")
    prix_h = models.CharField(max_length=512, blank=True, help_text="Prix hospitalier")
    prix_r = models.CharField(max_length=512, blank=True, help_text="Base de remboursement / PPV")

    etat   = models.CharField(max_length=512, blank=True, help_text="Statut")
    cnss   = models.CharField(max_length=512, blank=True, help_text="Remboursement")
    pcep   = models.CharField(max_length=512, blank=True, help_text="Princeps")
    tier   = models.CharField(max_length=512, blank=True, help_text="Tiers Payant")
    gros   = models.CharField(max_length=512, blank=True, help_text="Grossesse")

    def __str__(self): return str(self.alias)

    class Meta:
        verbose_name = "Medicament"
        verbose_name_plural = "Medicaments"

